FROM registry.hub.docker.com/google/cloud-sdk:alpine

RUN apk add --no-cache curl jq bash nodejs npm unzip zip
RUN npm i jsonlint -g

ADD functions.sh .