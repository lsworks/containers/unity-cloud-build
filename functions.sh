#!/bin/bash
[[ "$TRACE" ]] && set -x

#TODO: this will match file.json.meta files and cause problems. Refinement required.
function jsonlint_all_files() {
  IFS=$'\n'
  for FILE in $(find . -name "*.json"); do
    printf "Linting: %s\n" "${FILE}"
    jsonlint -q "${FILE}"
  done
}

function jsonlint_changed_files() {
  IFS=$'\n'
  for FILE in $(git diff-tree --no-commit-id --name-only -r HEAD | grep json$); do
    [[ -e "$FILE" ]] || exit 0
    printf "Linting: %s\n" "${FILE}"
    jsonlint -q "${FILE}"
  done
}

# Prepare the json files to be uploaded to the remote database
function prepare_json_files() {
  IFS=$'\n'
  if [[ ! -d "./uploads" ]]; then
    printf "Making uploads directory...\n"
    mkdir uploads
  fi

  for FILE in $(git diff-tree --no-commit-id --name-only -r HEAD HEAD~1 | grep json$); do
    if [[ ! -e "$FILE" ]]; then
      echo "No json files to edit."
      exit 0
    fi
    # Get the filename without the path
    FILENAME="${FILE##*/}"
    UPLOAD_NAME="uploads/${FILENAME}"
    # Create the required jsonfile for upload
    printf "Generating file: %s\n" "${UPLOAD_NAME}"
    printf "{\n" > "${UPLOAD_NAME}"
    printf "  \"gameFiles\": [\n" >> "${UPLOAD_NAME}"
    printf "    {\n" >> "${UPLOAD_NAME}"
    printf "    \"${FILENAME}\":\n" >> "${UPLOAD_NAME}"
    jq . "${FILE}" >> "${UPLOAD_NAME}"
    printf "    }\n" >> "${UPLOAD_NAME}"
    printf "  ]\n" >> "${UPLOAD_NAME}"
    printf "}" >> "${UPLOAD_NAME}"
  done
}

function prepare_all_json_files() {
  IFS=$'\n'
  if [[ ! -d "./uploads" ]]; then
    printf "Making uploads directory...\n"
    mkdir uploads
  fi

  for FILE in $(find . -name "*.json"); do
    # Get the filename without the path
    FILENAME="${FILE##*/}"
    UPLOAD_NAME="uploads/${FILENAME}"
    # Create the required jsonfile for upload
    printf "Generating file: %s\n" "${UPLOAD_NAME}"
    printf "{\n" > "${UPLOAD_NAME}"
    printf "  \"gameFiles\": [\n" >> "${UPLOAD_NAME}"
    printf "    {\n" >> "${UPLOAD_NAME}"
    printf "    \"${FILENAME}\":\n" >> "${UPLOAD_NAME}"
    jq . "${FILE}" >> "${UPLOAD_NAME}"
    printf "    }\n" >> "${UPLOAD_NAME}"
    printf "  ]\n" >> "${UPLOAD_NAME}"
    printf "}" >> "${UPLOAD_NAME}"
  done
}

function prepare_session_files() {
  IFS=$'\n'
  if [[ ! -d "./uploads" ]]; then
    printf "Making uploads directory...\n"
    mkdir uploads
  fi

  for FILE in $(find . -name "*-session-*.json"); do
    # Get the filename without the path
    FILENAME="${FILE##*/}"
    UPLOAD_NAME="uploads/${FILENAME}"
    # Create the required jsonfile for upload
    printf "Generating file: %s\n" "${UPLOAD_NAME}"
    printf "{\n" > "${UPLOAD_NAME}"
    printf "  \"gameFiles\": [\n" >> "${UPLOAD_NAME}"
    printf "    {\n" >> "${UPLOAD_NAME}"
    printf "    \"${FILENAME}\":\n" >> "${UPLOAD_NAME}"
    jq . "${FILE}" >> "${UPLOAD_NAME}"
    printf "    }\n" >> "${UPLOAD_NAME}"
    printf "  ]\n" >> "${UPLOAD_NAME}"
    printf "}" >> "${UPLOAD_NAME}"
  done

  for FILE in $(find . -name "*-speedreadingsession-*.json"); do
    # Get the filename without the path
    FILENAME="${FILE##*/}"
    UPLOAD_NAME="uploads/${FILENAME}"
    # Create the required jsonfile for upload
    printf "Generating file: %s\n" "${UPLOAD_NAME}"
    printf "{\n" > "${UPLOAD_NAME}"
    printf "  \"gameFiles\": [\n" >> "${UPLOAD_NAME}"
    printf "    {\n" >> "${UPLOAD_NAME}"
    printf "    \"${FILENAME}\":\n" >> "${UPLOAD_NAME}"
    jq . "${FILE}" >> "${UPLOAD_NAME}"
    printf "    }\n" >> "${UPLOAD_NAME}"
    printf "  ]\n" >> "${UPLOAD_NAME}"
    printf "}" >> "${UPLOAD_NAME}"
  done

  for FILE in $(find . -name "*-readingsession-*.json"); do
    # Get the filename without the path
    FILENAME="${FILE##*/}"
    UPLOAD_NAME="uploads/${FILENAME}"
    # Create the required jsonfile for upload
    printf "Generating file: %s\n" "${UPLOAD_NAME}"
    printf "{\n" > "${UPLOAD_NAME}"
    printf "  \"gameFiles\": [\n" >> "${UPLOAD_NAME}"
    printf "    {\n" >> "${UPLOAD_NAME}"
    printf "    \"${FILENAME}\":\n" >> "${UPLOAD_NAME}"
    jq . "${FILE}" >> "${UPLOAD_NAME}"
    printf "    }\n" >> "${UPLOAD_NAME}"
    printf "  ]\n" >> "${UPLOAD_NAME}"
    printf "}" >> "${UPLOAD_NAME}"
  done
}

function upload_gamefiles() {
  for FILE in uploads/*.json; do
    printf "Uploading file %s\n" "${FILE} to ${1}"
    curl -X POST -H "Content-Type: application/json" -d @"${FILE}" "${1}"
    printf "\n"
  done
}

function set_build_number() {
  if [[ -f "build.json" ]]; then
    BUILD_ID=$(jq .[].build build.json)
  else
    # If there isn't an ongoing build, then get the last build information
    printf "Missing \'build.json\', getting BUILD_ID of last build."
    curl -X GET --silent \
      -H "Content-Type: application/json" \
      -H "Authorization: Basic ${UNITY_API_KEY}" \
      "${UNITY_API_URL}" | jq .[0] > build.json
    BUILD_ID=$(jq .build build.json)
  fi
}

function start_build() {
  curl -X POST \
      -d '{"clean": true, "delay": 30}' \
      -H "Content-Type: application/json" \
      -H "Authorization: Basic ${UNITY_API_KEY}" \
      "${UNITY_API_URL}" > build.json
  if [[ -z "$BUILD_ID" ]]; then
    printf "\n\nCalling set_build_number from start_build\n\n"
    set_build_number
  fi
  printf "\nSTARTING BUILD NUMBER: %s\n" "${BUILD_ID}"
}

function get_build_status() {
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi
  BUILD_STATUS=$(curl -X GET --silent -H "Content-Type: application/json" -H "Authorization: Basic ${UNITY_API_KEY}" ${UNITY_API_URL}/${BUILD_ID} | jq .buildStatus)
}

function wait_for_build() {
  # Wait until the build is finished
  get_build_status

  while [[ ! ("$BUILD_STATUS" =~ success) || ("$BUILD_STATUS" =~ failure) ]]; do
    get_build_status
    if [[ "$BUILD_STATUS" =~ "success" ]]; then
      printf "%s\n" "$BUILD_STATUS"
    else
      printf '.'
    fi

    if [[ $BUILD_STATUS =~ failure ]]; then
      printf "\nERROR: Last build failed. Aborting...\n"
      get_build_logs
      exit 1
    fi

    if [[ $BUILD_STATUS =~ canceled ]]; then
      printf "\nERROR: Last build was cancelled. Aborting...\n"
      get_build_logs
      exit 1
    fi

    sleep 30
  done
}

function get_build_logs() {
  # Get build logs
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi
  curl -X GET --silent -H "Content-Type: application/json" -H "Authorization: Basic ${UNITY_API_KEY}" "${UNITY_API_URL}/${BUILD_ID}/log" > unity_build.log
  tail unity_build.log
  printf "For more log informtaion, look for the 'unity_build.log' in the build artifacts..."
}

function unity_api_call() {
  # Get the download URL
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi
  curl -X GET --silent \
       -H "Content-Type: application/json" \
       -H "Authorization: Basic ${UNITY_API_KEY}" \
       "${UNITY_API_URL}/${BUILD_ID}"
}

function download_build() {
  # Get the download URL
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi
  UNITY_ASSETS=$(unity_api_call | jq -r .links.artifacts[].files[0])
  len=$(unity_api_call | jq '.links.artifacts[].files[0] | length' | wc -l)
  echo "len is $len"
  for (( asset=0; asset<len; asset++ ))
  do
    ASSET_FILENAME=$(basename $(unity_api_call | jq -r .links.artifacts["$asset"].files[0].filename))
    echo "ASSET_FILENAME is ${ASSET_FILENAME}"
    ASSET_URL=$(unity_api_call | jq -r .links.artifacts["$asset"].files[0].href)
    echo "ASSET_URL is ${ASSET_URL}"
    # ASSET_MD5SUM=$(unity_api_call | jq -r .links.artifacts["$asset"].files[0].md5sum)
    curl "${ASSET_URL}" -o "/tmp/${ASSET_FILENAME}"
  done
}

function unarchive_build() {
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi
  UNITY_ASSETS=$(unity_api_call | jq -r .links.artifacts[].files[0])
  len=$(unity_api_call | jq '.links.artifacts[].files[0] | length' | wc -l)
  for (( asset=0; asset<len; asset++ ))
  do
    ASSET_FILENAME=$(basename $(unity_api_call | jq -r .links.artifacts["$asset"].files[0].filename))
    ASSET_URL=$(unity_api_call | jq -r .links.artifacts["$asset"].files[0].href)
    # ASSET_MD5SUM=$(unity_api_call | jq -r .links.artifacts["$asset"].files[0].md5sum)

    # Get the primary asset filename as we will unarchive the the asset bundles into
    # it's path. The path matches the filename without extension.
    filename=$(unity_api_call | jq '.links.artifacts[] | select(.key=="primary")' | jq -r .files[0].filename)
    ASSET_PATH="${filename%.*}"
    # Get the archive size
    FILESIZE=$(stat -c%s "/tmp/${ASSET_FILENAME}")
    echo "$FILESIZE"
    # Test if this file size is large, if yes "fix" it first.
    if (( FILESIZE > 1000000000 ))
    then
      echo "Running zip file fix"
      cd /tmp && zip -FF "${ASSET_FILENAME}" --out fixed.zip
      echo "Unzipping fixed.zip"
      unzip -o fixed.zip
    else
      echo "Unzipping file"
      cd /tmp && unzip -o "${ASSET_FILENAME}"
    fi
    # Unarchive the Addressable Assets if present
    if [[ ! -d "/tmp/${ASSET_PATH}/StreamingAssets/" ]]
    then
      mkdir -p "/tmp/${ASSET_PATH}/StreamingAssets"
    fi
    if [[ -f /tmp/addressable_content.zip ]]
    then
      echo "Unzipping Addressable Assets"
      cd "/tmp/${ASSET_PATH}/StreamingAssets/" && unzip -o /tmp/addressable_content.zip
    fi
  done
}

function copy_to_server() {
  if [[ -z "$BUILD_ID" ]]; then
    set_build_number
  fi

  if [[ -d "/tmp/${ASSET_PATH}" ]]; then
    cd "/tmp/${ASSET_PATH}" && gsutil -m cp -r -- * "${1}"
  else
    unarchive_build
    cd "/tmp/${ASSET_PATH}" && gsutil -m cp -r -- * "${1}"
  fi
}
